var gulp = require('./gulp')([
  'clean',
  'babel'
]),
    runSequence = require('run-sequence');

gulp.task('build', function(cb) {
  runSequence(['clean', 'babel'], cb)
});
