var gulp = require('gulp'),
    babel = require('gulp-babel');

var es6to5Task = function() {
  return gulp.src('./src/js/app.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('build'));
};

gulp.task('es6to5', es6to5Task)
module.exports = es6to5Task
